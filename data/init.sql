-- CREATE DB
DROP DATABASE IF EXISTS GDC;
CREATE DATABASE GDC;
USE GDC;

-- CREATE TABLE for ads
DROP TABLE IF EXISTS `ad`;
CREATE TABLE `ad` (
	`id` BIGINT(20) NOT NULL,
	`owner_id` INT(11) NOT NULL,
	`title` VARCHAR(255) NOT NULL,
	`category` VARCHAR(50) NOT NULL,
	`price` DECIMAL(10,2) NOT NULL,
	`city` VARCHAR(255) NOT NULL,
	`created_at` DATE NOT NULL,
	`deleted_at` DATE NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `owner_id` (`owner_id`)
);
-- LOAD CSV DATA FILE INTO THE AD TABLE, BY IGNORING THE DUPLICATED ID FIELD, AND BY CLEANING THE CATEGORIES 
LOAD DATA LOCAL INFILE '/ads.csv' 
INTO TABLE ad 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS
(id,owner_id,title,@category,price,city,created_at,deleted_at)
SET 
category = REPLACE(@category,"_",""),
category = REPLACE(category,"relestate","realestate"),
category = LCASE(category);

-- CREATE TABLE for ads transactions
DROP TABLE IF EXISTS `ads_transactions`;
CREATE TABLE `ads_transactions` (
	`id` BIGINT(20) NOT NULL,
	`ad_owner_id` INT(11) NOT NULL,
	`buyer_user_id` INT(11) NOT NULL,
	`ad_id` INT(11) NOT NULL,
	`sold_price` DOUBLE NULL DEFAULT NULL,
	`created_at` DATE NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `ad_owner_id` (`ad_owner_id`),
	INDEX `ad_id` (`ad_id`),
	INDEX `buyer_user_id` (`buyer_user_id`)
);

-- LOAD CSV DATA FILE INTO THE ads_transactions TABLE
LOAD DATA LOCAL INFILE '/ads_transaction.csv' 
INTO TABLE ads_transactions
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

-- CREATE TABLE for users
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
	`id` BIGINT(20) NOT NULL,
	`age` INT(11) NULL DEFAULT NULL,
	`birthdate` DATE NULL DEFAULT NULL,
	`city` VARCHAR(255) NULL DEFAULT NULL,
	`created_at` DATE NOT NULL,
	`sex` CHAR(1) NULL DEFAULT NULL,
	`lat` DOUBLE NULL DEFAULT NULL,
	`long` DOUBLE NULL DEFAULT NULL,
	`PASSWORD` VARCHAR(255) NOT NULL,
	`utm_source` VARCHAR(100) NULL DEFAULT NULL,
	`utm_medium` VARCHAR(100) NULL DEFAULT NULL,
	`utm_campaign` VARCHAR(100) NULL DEFAULT NULL,
	`firstname` VARCHAR(100) NULL DEFAULT NULL,
	`lastname` VARCHAR(100) NULL DEFAULT NULL,
	`user_agent` VARCHAR(255) NULL DEFAULT NULL,
	`misc` LONGTEXT NULL,
	PRIMARY KEY (`id`)
);

-- LOAD CSV DATA FILE INTO THE users TABLE, BY IGNORING THE DUPLICATED ID FIELD, AND BY REPLACING THE EMPTY FIELDS BY NULL 
LOAD DATA LOCAL INFILE '/users.csv' 
INTO TABLE user 
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS
(@dummy,id,age,birthdate,city,created_at,@sex,@lat,@long,`PASSWORD`,@utm_source,@utm_medium,@utm_campaign,firstname,lastname,user_agent,misc)
SET
sex = NULLIF(@sex,''),
lat = NULLIF(@lat,''),
`long` = NULLIF(@long,''),
utm_source = NULLIF(@utm_source,''),
utm_medium = NULLIF(@utm_medium,''),
utm_campaign = NULLIF(@utm_campaign,'');

-- CREATE TABLE for referrals
DROP TABLE IF EXISTS `referral`;
CREATE TABLE `referral` (
	`id` BIGINT(20) NOT NULL,
	`referrer_user_id` INT(11) NOT NULL,
	`referree_user_id` INT(11) NOT NULL,
	`created_at` DATE NOT NULL,
	`deleted_at` DATE NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `referree_user_id` (`referree_user_id`),
	INDEX `referrer_user_id` (`referrer_user_id`)
);

-- LOAD CSV DATA FILE INTO THE referral TABLE
LOAD DATA LOCAL INFILE '/referrals.csv' 
INTO TABLE referral
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;