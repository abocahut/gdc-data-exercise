## Introduction
In this project we will see how to load the data files from GDC into a mysql database. I chose MySQL as it is a very common tool to start a simple BI project, and can be later moved to a Redshift instance if we need to scale up. MySQL is robust for midium-size datasets, and very easy to set up. Besides, the data we have to deal with is structured, strongly typed, has relationnal keys, and thus can be easily mapped to a RDBMS model.
Finally, MySQL is SQL compliant, which is great to achieve a lot of analytics needs, and it comes with a powerful engine that can load and transform csv files.

You can also find in this repository a file report `analytics-report-gdc.md`, where I highlighted a few business indicators with anaytics charts.

---

## Init database

Let's start with running a MySQL server with the latest image from docker. The `local-infile` arg is used to load csv files through the MySQL `LOAD DATA INFILE` command.

```
docker run --name gdc-db -e MYSQL_ROOT_PASSWORD=pass123! -d mysql:latest --local-infile=1
```



We can now copy our csv files and sql init script on our db container :

```
docker cp /data/. gdc-db:/
```

Finally, we start a client and launch the init script, which will load the csv files into the database. Note that we also pass the `local-infile` arg, in order to push the data from client to server. I could have written the script in java, but I prefer to use the native MySQL `LOAD DATA INFILE` command, which is very easy to use and to maintain. Simple data cleaning and transformations can be achieved through common SQL commands, that anybody can read and understand.
```
docker exec gdc-db /bin/sh -c 'mysql -h127.0.0.1 -uroot -ppass123! --local-infile=1 </init.sql'
```

**And that's it. You can now connect your favorite client to the db container to query the data.**

---
