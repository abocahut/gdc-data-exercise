# Data Analysis

You will find in this report some indicators that showed up when I started to analyze the sample of data in the GDC database. Please note that this is a draft work, and must be confirmed by a full production dataset.

# Users

Users mainly come from facebook campaigns. Maybe CRM investments should focus on new acquisition sources.
The acquisition rate of new customers is quite constant over the past 4 years, but strongly decreases since March 2020. This is probably due to the coronavirus crisis, so it is necessary to prepare a massive campaign to relaunch the activity.

![](assets/users_creations_by_utm_source.png)

# Transactions
Transactions are very seasonnal. There is a small peak in December, a huge peak in May and a trough in March. This is probably due to the categories of the ads, which are only focused on vehicules and real estate. Maybe GDC services should also address new categories of ads, like baby-sitting, hair dressing, massages... for which the demand is more constant through the year.


![](assets/ads_trx_dates.png)
![](assets/ads-category.png)

